package Source;


import java.util.Timer;
import java.util.TimerTask;

/**
 * a timer for notifying Programmer threads
 * @author e1746320
 */
public class ScrumMaster extends Timer{
    
	public static int remainingProgrammers = 0; //you can use this as a barrier to synchronize the threads.
	public static boolean scrumDone = false;
	public static boolean newDay = false;
    public static int day;
    public static PeriodicIncrement timertask;
    private Timer instance;
    
    public class PeriodicIncrement extends TimerTask {

		@Override
		public void run() {
			
			System.err.println("Day "+day+":");
			day += 1;
			synchronized (this) {
				newDay = true;
				this.notifyAll();
			}
			
			if ( remainingProgrammers <= 0 ){
				scrumDone = true;
				System.err.println("SCRUM Finished. It took "+(day-2)+" days in total");
				
				instance.cancel();
				instance.purge();
				return;	
			}
		}
    }
	ScrumMaster () {
        super ();
        this.timertask = new PeriodicIncrement();
        this.day = 0;
        this.schedule(timertask, 0, 100);
        this.remainingProgrammers = Homework2.numberOfProgrammers;
        this.instance = this;
    }
}
