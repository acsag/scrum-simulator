/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Source;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class TestCaseCreator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        String fName = "HW2Input.txt";
        int programmers = 4, taskCnt = 20, taskTypes = 4, seed = 1, queueSize = 8, talentCnt = 8;
        Random rng = new Random (seed);
        PrintWriter f = new PrintWriter(fName);
        f.format("%d %d %d %d\n", taskCnt, taskTypes, queueSize, programmers);
        for (int i = 0; i < programmers; i++) {
            int t1, t2, t3;
            t1 = rng.nextInt(talentCnt);
            t2 = rng.nextInt(talentCnt);
            t3 = rng.nextInt(talentCnt);
            while (((t1 > 3) && (t2 >3) && (t3>3)) || (t1 == t2) || t1 == t3 || t2 == t3) { //at least one of them should be 0-3
                t1 = rng.nextInt(talentCnt);
                t2 = rng.nextInt(talentCnt);
                t3 = rng.nextInt(talentCnt);
            }
            f.format("%d %d %d \n", t1, t2, t3);
        }
        for (int i = 0; i <taskCnt; i++) {
            f.format("%d %d ", rng.nextInt(4), 1+rng.nextInt(10));
        }
        f.close();
        
    }
    
}
