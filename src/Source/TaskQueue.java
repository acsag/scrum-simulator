
package Source;


import java.util.ArrayList;
import java.util.Iterator;

/**
 * A synchronized queue for threads
 * @author e1746320
 */
public class TaskQueue implements Iterable {
	//Homework's task classes are defined as inner classes of TaskQueue
    //<editor-fold defaultstate="collapsed" desc="Inner Classes (Tasks)">
	
	private static TaskQueue instance = null;
	private final ArrayList<Task> taskList = new ArrayList<Task>();
	private final ArrayList<Task> oldList = new ArrayList<Task>();
    private static int capacity = 0;
    public static Object queueLock = new Object();
    public int length = 0;
    
    private TaskQueue() {
    }
    
	public static TaskQueue getInstance() {
		if (instance == null) {
			instance = new TaskQueue();
		}
		return instance;
	}
	
	public synchronized void printQueue() {
		System.err.print("Queue: ");
		for (Task t : taskList ) {
			String s = t.getClass().getSimpleName();
			System.err.print(s.substring(4));
		}
		/*// $DEBUG		// prints a list of done tasks
		System.err.print(" :::: ");
		for (Task t : oldList ) {
			String s = t.getClass().getSimpleName();
			System.err.print(s.substring(4));
		}
		System.err.println("--- "+oldList.size()+" tasks in total");*/
		System.err.println("");
	}
	
	public void putTask(Task t) {
		synchronized (this.instance) {
			taskList.add(t);
			length++;
		}
	}
	
	public synchronized Task getTask(int i) {
		if (i<length) {
			return taskList.get(i);
		}
		else return null;
	}
	public synchronized Task takeTask(int i) {
		if (i>=0 && i<length) {
			this.instance.notifyAll();
			length--;
			Task t = taskList.remove(i);
			oldList.add(t);// $DEBUG
			return t;
			//return taskList.remove(i);
		}
		return null;
	}
	
	/**
	 * checks whether the queue is full or not
	 * @return true for full queue length
	 */
	public synchronized boolean isFull() {
		return (length < capacity ) ? false : true;
	}
	
	/**
	 * checks emptiness of queue
	 * @return true for empty queue
	 */
	public synchronized boolean isEmpty() {
		return (length == 0);
	}
    
	/**
	 * implements Iterable interface
	 * 	is necessary for traversing with foreach loop
	 */
	@Override
	public Iterator<Task> iterator() {
		return new Iterator<Task>() {
			
			private int currentPos = 0;
			
			@Override
			public boolean hasNext() {
				synchronized (TaskQueue.instance) {
					
					if ( ++currentPos < taskList.size()) {
						return true;
					}
					else {
						return false;
					}
				}
			}
			@Override
			public Task next() {
				synchronized (TaskQueue.instance) {
					Task t = taskList.get(currentPos);
					return t;
				}
			}
	
			@Override
			public void remove() {
				taskList.remove(currentPos);
				synchronized(this) {
					queueLock.notify();
				}
			}
				
		};
	}
	
	
	public static int getCapacity() {
		return capacity;
	}

	public static void setCapacity(int capacity) {
		TaskQueue.capacity = capacity;
	}
	
	/** 
	 * returns Task ID of a Task object 
	 * @param t: Task object to get its ID
	 * @return ID of the task
	 */
	public static int getTaskIDof(Task t) {
		String tName = t.getClass().getSimpleName();
		return Integer.parseInt(tName.substring(4));
	}
	public static class Task {
        
        boolean finished;
        int duration;
        
        Task(int duration) {
        		this.duration = duration;
        		finished = false;
            }
        }
    
    public static class Task0 extends Task {
        
        public int f01;
        public ArrayList<Integer> f02;
        
        public Task0(int duration) {
            super(duration);
        }
        
        public void m01(int a1) {
            
        }
        
        public int m02() {
            return 1;
        }
    }
    
    public static class Task1 extends Task {
        
        private boolean f11;
        
        public Task1(int duration) {
            super(duration);
        }
        
        private void m11() {
            
        }
    }
    
    public static class Task2 extends Task implements TaskInterface {
        
        public Task2(int duration) {
            super(duration);
        }
        
        @Override
        public void m21() {
            
        }
    }
    
    public static class Task3 extends Task {
        
        public final boolean f31 = false;
        
        public final double m31() {
            return 0.0;
        }
        
        public Task3(int duration) {
            super(duration);
        }
        
    }
    
    interface TaskInterface {
        
        void m21();
    }
}

