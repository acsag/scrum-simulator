package Source;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * a class for simulating Scrum environment
 * 	implemented for METU CENG443 course Homework2
 * 	
 * @author e1746320, Ali Can SAG
 * email: e1746320@ceng.metu.edu.tr
 */
public class Homework2 {
	
	private static int numberOfTasks;
	public static int numberOfProgrammers;
	private static int sizeOfQueue;
	private static int numberOfTaskTypes;
	
	private static ArrayList<Programmer> listProgrammer = new ArrayList<Programmer>();
	private static ScrumMaster scrumMaster;
	private static ProductOwner productOwner;
	
	/**
	 * reads files according to Homework specification
	 * @param inputFileLoc: a string which shows the file's location
	 */
	private static void readFiles(String inputFileLoc) {
		Scanner sc = null;
		try {
			//System.out.println(System.getProperty("user.dir")); //$DEBUG
			sc = new Scanner(new File(inputFileLoc));
			
			numberOfTasks = sc.nextInt();
			numberOfProgrammers = sc.nextInt();
			sizeOfQueue = sc.nextInt();
			numberOfTaskTypes = sc.nextInt();
			
			
			// $DEBUG
			System.out.println("numberOfTasks: "+numberOfTasks);
			System.out.println("numberOfProgrammers: "+numberOfProgrammers);
			System.out.println("sizeOfQueue: "+sizeOfQueue);
			System.out.println("numberOfTaskTypes: "+numberOfTaskTypes);
		
			
			productOwner = new ProductOwner(numberOfTasks);
	
			for (int i=0; i<numberOfProgrammers ; ++i) {
				int[] talents = new int[3]; 
				talents[0] = sc.nextInt();
				talents[1] = sc.nextInt();
				talents[2] = sc.nextInt();
				Programmer p = new Programmer(i+1,talents);
				listProgrammer.add(p);
			
				//  $DEBUG
				System.out.println("Programmer Thread-"+(i+1)+" with Talents: "+talents[0]+","+talents[1]+","+talents[2]);
			
			}
			
			// $DEBUG
			System.out.print("TASK LIST: ");
			
			for (int i=0; i<numberOfTasks; ++i) {
				int type = sc.nextInt();
				int dur = sc.nextInt();
				productOwner.addTask(type, dur);
				System.out.print(type); // $DEBUG
			}
			
			System.out.println(" :: "+numberOfTasks+" tasks in total"); //$DEBUG	
		
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (sc != null) {
				sc.close();
			}
		}
	}
	

	public static void main(String[] args) {
		
		Homework2.readFiles( args[0] );
		System.out.println("------ Scrum Simulation Starting");
		Homework2.simulate();
	}
	
	/**
	 * Simulates whole Scrum team by creating threads and waiting
	 * 	them to finish.
	 */
	public static void simulate() {
		
		TaskQueue.setCapacity(sizeOfQueue);
		
		productOwner.start();
		scrumMaster = new ScrumMaster();
		for (Programmer p : listProgrammer) {
			p.start();
		}
	}

}
