package Source;

import java.util.LinkedList;

import Source.TaskQueue.*;


/**
 * a thread whose main function is to add task whenever queue has a empty slot
 * 	It is notified by queue  
 * @author e1746320
 */
public class ProductOwner extends Thread{
    
    LinkedList<Task> tasks;
    int taskCnt;
    TaskQueue queue;
    
    
    ProductOwner (int taskCnt) {
        this.taskCnt = taskCnt;
        tasks = new LinkedList<Task>();
        this.queue = TaskQueue.getInstance();
    }
    

	@Override
	public void run() {
		super.run();
		while ( !tasks.isEmpty() ) {
			synchronized ( queue ) {
	    		while (queue.isFull()) {
	    			try {
	    				queue.wait();
	    			} catch (InterruptedException e) {
	    				e.printStackTrace();
	    			}
	    		}
	    		Task t = tasks.pop();
	    		queue.putTask(t);
	    		System.err.println("Product owner put a task of type "+t.getClass().getSimpleName()+" on queue. "+ (tasks.size() ) +" tasks left");
			}
    	}
	}

	//You can use this method to add task types that you read from the file
    void addTask(int t, int duration) {
        switch (t) {
                case 0: tasks.add(new Task0(duration));
                    break;
                case 1: 
                	tasks.add(new Task1(duration));
                    break;
                case 2: tasks.add(new Task2(duration));
                    break;
                case 3: tasks.add(new Task3(duration));
                    break;
            }
    }
}
