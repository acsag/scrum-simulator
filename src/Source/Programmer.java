package Source;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;

import Source.TaskQueue.*;

/**
 * a class which tries to do all the task in the TaskQueue according to its talents
 * @author e1746320
 */
public class Programmer extends Thread {
	
    private TaskQueue queue;
    private int programmerID;
    private Task assignedTask;
    private int taskEndDay;
    private boolean finished;		// for exiting the application
    private ArrayList<Talent> talents = new ArrayList<>();
    
    // for optimization while checking with Reflection API
    private ArrayList<Field> talentFields;
    private ArrayList<Method> talentMethods;
    
    
	Programmer(int programmerID_, int[] talents_) {
		
		this.queue = TaskQueue.getInstance();
		this.programmerID = programmerID_;
		this.assignedTask = null;
		this.taskEndDay = 0;
		this.finished = false;
		this.addTalent(talents_[0]);
		this.addTalent(talents_[1]);
		this.addTalent(talents_[2]);
		this.talentFields = new ArrayList<Field>();
		this.talentMethods = new ArrayList<Method>();
		getTalentFieldsAndMethods();
    }
	
	/**
	 * precomputes Reflection API check for talent and task match
	 * 	field and methods are put into arraylists for easier iteration
	 */
	private void getTalentFieldsAndMethods() {
		for (int i=0; i<talents.size(); ++i) {
			Class tClss = talents.get(i).getClass();
			Field[] fields = tClss.getFields();
			talentFields.addAll ( Arrays.asList(fields) );
			Method[] methods = tClss.getMethods();
			talentMethods.addAll( Arrays.asList(methods));
		}
	}

	/**
	 * prints status of the queue whenever ScrumMaster changes the flag newDay (i.e, in every tick of TimerTask) 
	 */
	private void newDayPrinting() {
		synchronized (queue) {
			if ( ScrumMaster.newDay ) {
				ScrumMaster.newDay = false;
				queue.printQueue();
			}
		}
	}
	
	/**
	 * a class for counting and printing statistics about tasks of the programmer
	 * @author e1746320
	 * 
	 */
	public class TaskStats {
		public int tasksTotal[] = {0,0,0,0};
		public int daysTotal[] = {0,0,0,0};
		public int id = 0;

	    
		public TaskStats(int id_) {
			id = id_;
		}
		public void printTaskStats() {
			int totTask = 0;
			int totDays = 0;
			for ( int i=0; i<4 ; ++i ) {
				totTask += tasksTotal[i];
				totDays += daysTotal[i];
			}
			System.err.println("Programmer Thread-"+id+", worked on "+totTask+" tasks in total for "+totDays+" days");
			for (int i=0; i<4; ++i) {
				if (tasksTotal[i] == 0) {
					continue;
				}
				System.err.println("	Task"+i+":	"+tasksTotal[i]+" tasks,	"+daysTotal[i]+" days");
			}
		}
		public void update(Task task) {
			int taskID = TaskQueue.getTaskIDof(task);
			
			if (taskID < 0 || taskID > 7)
				taskID = 7;
			tasksTotal[taskID]++;
			daysTotal[taskID] += task.duration;
		}
	}
	
	/**
	 * checks whether Task object has same fields & methods as Talent objects of the programmer 
	 * @param task: any Task object from TaskQueue 
	 * @return a boolean: true if Task object can be handled by programmer
	 */
	private boolean compareTalentsWithTask(Task task) {
		
		Field[] taskFields = task.getClass().getFields();
		Method[] taskMethods = task.getClass().getMethods();
		
		// --------------------- check every field of the task with every talents' fields 
		boolean found = true;
		for ( Field taskField: taskFields) {
			found = false;
			for ( Field talentField : talentFields ) {
				if (taskField.getName().equals(talentField.getName()) && 
						( taskField.getModifiers() == talentField.getModifiers() ) ) {
					found = true;
					break;
				}
			}
			if (!found) {
				break;
			}
		}
		if (!found) return false;
		
		// --------------------- check every method of the task with every talents' methods
		for ( Method taskMethod : taskMethods) {
			found = false;
			for ( Method talentMethod: talentMethods) {
				if (taskMethod.getName().equals(talentMethod.getName()) && 
						( taskMethod.getModifiers() == talentMethod.getModifiers() ) ) {
					found = true;
					break;
				}
			}
			if (!found) {
				break;
			}
		}
		if (!found) return false;
		return true;	
	}
	
    @Override
	public void run() {
		super.run();
		TaskStats stats = new TaskStats(programmerID);

		while(true) {
			synchronized (ScrumMaster.timertask) {
				try {
					if (ScrumMaster.scrumDone) {	// if all finished, print and exit
						stats.printTaskStats();
						return;
					}
					if ( ScrumMaster.day < taskEndDay ) {	// do the job until the end day
						while (ScrumMaster.day <= taskEndDay) {
							ScrumMaster.timertask.wait();
							newDayPrinting();
						}
						taskEndDay = 0;
						stats.update(assignedTask);
						System.err.println("Programmer Thread-"+programmerID+" finished the "+assignedTask.getClass().getSimpleName()+" at hand.");
					}
					ScrumMaster.timertask.wait();
					newDayPrinting();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
			}
			

			if ( queue.isEmpty() ) { 	// wait for a task to be added OR tasks to be finished OR application to exit
				if (finished == false) { // finished flag makes sure executing here only once   
					ScrumMaster.remainingProgrammers--;
					finished = true;
				}
			}
			else {
				synchronized (queue) {
					for(int i=0; i<queue.length; ++i ) {
						if ( compareTalentsWithTask(queue.getTask(i)) ) {	// check if a task compatible with this programmer's talent
							assignedTask = queue.takeTask(i);
							
							taskEndDay = assignedTask.duration + ScrumMaster.day;
							System.err.println("+ Programmer Thread-"+programmerID+" started working on "+assignedTask.getClass().getSimpleName() + " (ends on day "+ taskEndDay+ ")");
							break;
						}
					}
					if (assignedTask == null) {
						System.err.println(" Programmer Thread-"+programmerID+" has no task appropriate, waiting...");
					}
				}
			}
		}
	}

	public static class Talent {
    
    }
    
    public static class Talent0 extends Talent {
        
        public int f01;
        public ArrayList<Integer> f02;
        
        public void m01(int a1) {
            
        }
        
        public int m02() {
            return 0;
        }
    }
    
    public static class Talent1 extends Talent {
        
        private boolean f11;
        
        private void m11() {
        }
        
    }
    public static class Talent2 extends Talent implements TaskInterface {
        
        @Override
        public void m21() {
            
        }
        
    }

    public static class Talent3 extends Talent {
        
        public final boolean f31 = true;
        
        public final double m31() {
            return 0.0;
        }
        
    }
    
    private static class Talent4 extends Talent {
		
		public int f01;
		public ArrayList<Integer> f02;
		
		public final double m31() {
			return 0.0;
        }
    }
    
    private static class Talent5 extends Talent {
		
		public final boolean f31 = true;
		
		public void m01(int a1) {
            
        }
        
        public int m02() {
            return 0;
        }
	}
    
    private static class Talent6 extends Talent {
        private void m11() {
        }
    }
    
    private static class Talent7 extends Talent {
        private boolean f11;
    }

    public void addTalent(int t) {
        switch (t) {
            case 0:
                talents.add(new Talent0());
                break;
            case 1:
                talents.add(new Talent1());
                break;
            case 2:
                talents.add(new Talent2());
                break;
            case 3:
                talents.add(new Talent3());
                break;
            case 4:
                talents.add(new Talent4());
                break;
            case 5:
                talents.add(new Talent5());
                break;
            case 6:
                talents.add(new Talent6());
                break;
            case 7:
                talents.add(new Talent7());
                break;
        }
    }
}
