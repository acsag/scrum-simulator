package denemeler;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import Source.Programmer.Talent;
import Source.Programmer.Talent0;
import Source.TaskQueue.Task;
import Source.TaskQueue.Task0;

public class ReflectanceDeneme {
	
	private static void printReflections() {
		Task task = new Task0(0);
		Talent talent = new Talent0();

		Class taskCl = task.getClass();
		Class talentCl = talent.getClass();
		
		Field[] taskFi = taskCl.getFields();
		Method[] taskMe = taskCl.getMethods();
		
		for ( Field f : taskFi) {
			//System.out.println("Task Field: "+f);
			int modifs = f.getModifiers();
			System.out.print(modifs);
			String simpleName = f.getName();
			System.out.println(", "+simpleName);
		}
		
		System.out.println("----");
		
		Field[] taskFi2 = talentCl.getFields();
		Method[] taskMe2 = talentCl.getMethods();
		
		for ( Field f : taskFi2) {
			//System.out.println("Talent Field: "+f);
			int modifs = f.getModifiers();
			System.out.print(modifs);
			String simpleName = f.getName();
			System.out.println(", "+simpleName);
		}
		
		System.out.println("++++++++");
		
		for ( Method m : taskMe) {
			//System.out.println("Task Method: "+m);
			int modifs = m.getModifiers();
			System.out.print(modifs);
			String simpleName = m.getName();
			System.out.println(", "+simpleName);
		}

		System.out.println("----");
		
		for ( Method m : taskMe2) {
			//System.out.println("Task Method: "+m);
			int modifs = m.getModifiers();
			System.out.print(modifs);
			String simpleName = m.getName();
			System.out.println(", "+simpleName);
		}
	}
	
	
	private static boolean compareReflections() {
		
		boolean found = false;
		
		Task task = new Task0(0);
		Talent talent = new Talent0();

		Class taskCl = task.getClass();
		Class talentCl = talent.getClass();
		
		Field[] taskFi = taskCl.getFields();
		Field[] taskFi2 = talentCl.getFields();
		
		for ( Field f1 : taskFi) {
			int f1modifs = f1.getModifiers();
			System.out.print("for "+f1modifs);
			String f1simpleName = f1.getName();
			System.out.println(", "+f1simpleName);
			
			found = false;
			for ( Field f2 : taskFi2 ) {
				int f2modifs = f2.getModifiers();
				System.out.print("	"+f2modifs);
				String f2simpleName = f2.getName();
				System.out.println(", "+f2simpleName);
				
				if ( f1simpleName.equals(f2simpleName) &&
						( f1modifs == f2modifs )
						) {
					found = true;
					break;
				}
				
			}
			if (!found) {
				System.out.println("FIELD NOT FOUND!");
				return found;
			}
		}
		
		System.out.println("field is in there!");
		System.out.println("----");
		
		Method[] taskMe = taskCl.getMethods();
		Method[] taskMe2 = talentCl.getMethods();
		
		for ( Method m1 : taskMe) {
			int m1modifs = m1.getModifiers();
			System.out.print("for "+m1modifs);
			String m1simpleName = m1.getName();
			System.out.println(", "+m1simpleName);
			Class m1ret = m1.getReturnType();
			Class[] m1param = m1.getParameterTypes();
			
			found = false;
			for ( Method m2 : taskMe2 ) {
				int m2modifs = m2.getModifiers();
				System.out.print("	"+m2modifs);
				String m2simpleName = m2.getName();
				System.out.println(", "+m2simpleName);
				Class m2ret = m2.getReturnType();
				Class[] m2param = m2.getParameterTypes();
				
				if ( m1simpleName.equals(m2simpleName) &&
						( m1modifs == m2modifs ) &&
						m1ret.equals(m2ret)
						) {
					
					found = true;
					// compare parameters
					for ( Class param1 : m1param ) {
						for ( Class param2 : m2param ) {
							if ( !param1.equals(param2) ) {
								found = false;
								System.out.println("-----NOO :(");
								System.out.println("		m1: "+param1);
								System.out.println("		m2: "+param2);
							}
							else {
								found = true;
								break;
							}
						}
						if (!found) {
							System.out.println("-----NOOxxxxx2 :(");
							break;
						}
					}
					if (found) {
						break;
					}
				}
			}
			if (!found) {
				System.out.println("METHOD NOT FOUND!");
				return found;
			}
		}
		return found;
	}
	
	public static void main(String[] args) {
		// printReflections();
		if (compareReflections() ) {
			System.out.println("Success");
		}
		else {
			System.err.println("FAIL!");
		}
		
	}
}
