package denemeler;

import java.util.Timer;
import java.util.TimerTask;

public class MultithreadTimer extends Timer {
	static int day = 0;
	
	private static class PeriodicIncr extends TimerTask {
				
		@Override
		public void run() {
			System.out.println("Tick"+day+":");
			day += 1;
		}
	}
	
	public static void main(String[] args) {
		Timer timer = new MultithreadTimer(); 
		TimerTask timerTask = new PeriodicIncr();
		
		timer.scheduleAtFixedRate(timerTask, 0, 3*1000); // in every 3 sec
		System.out.println("TimerTask started");
		try {
			Thread.sleep(120000);
		} catch ( InterruptedException e) {
			e.printStackTrace();
		}
		timer.cancel();
		System.out.println("TimerTask cancelled");
		try {
			Thread.sleep(30000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
