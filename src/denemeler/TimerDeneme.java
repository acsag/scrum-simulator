package denemeler;

import java.util.Timer;
import java.util.TimerTask;

public class TimerDeneme {
	
	Timer timer;
	
	public TimerDeneme(int seconds) {
		timer = new Timer();
		timer.schedule(new RemindTask(), seconds * 500);
	}
	
	private class RemindTask extends TimerTask {
		public void run() {
			System.out.println("wiggle wiggle wiggle...");
			timer.cancel();
		}
	}
	
	public static void main(String[] args) {
		System.out.println("--- About to schedule a task ---");
		new TimerDeneme(5);
		System.out.println("--- Task scheduled ---");
	}

}
