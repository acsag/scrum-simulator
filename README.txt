CENG443-HW2
Ali Can SAG
1746320


Under Docs folder:
	* documentation (html) files for each class, created by "javadocs" command
	* taskDistribution.txt file for trying to answer the homework's bonus question

Under Source folder:
	* Homework2.java
	* ProductOwner.java
	* Programmer.java
	* ScrumMaster.java
	* TaskQueue.java


Example execution in the folder hw2/ in linux: ( where inputs are in hw2/ directory )
	$ javac Source/*.java
	$ java -classpath ./ Source/Homework2 input1.txt
	
